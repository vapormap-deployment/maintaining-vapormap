#!/bin/bash
echo "Place AWS access ID and Key in aws-cred-export.sh file"
source ./aws-cred-export.sh
echo "Use plan | apply | destroy"
mkdir tmp
terraform -chdir=IaC $1

if [ "$1" = "apply" ]; then
    export hosts=$(cat tmp/public_ips.txt)
    cat templates/hosts.template | envsubst '${hosts}' > playbook/hosts
    ansible-playbook playbook/runner.yml -i playbook/hosts
fi
sleep 5
rm -rf tmp