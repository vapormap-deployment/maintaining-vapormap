# Rapport Fil Rouge 4
Ali HAMOUDA

## Introduction
L'objectif de ce projet est de déployer en utilisant un pipeline l'application **Vapormap**.

## Architecture cible
Le but est de déployer **Vapormap** sur une instance sur EC2. J'ai choisi de travailler avec Microk8s comme orchestrateur.
Il était utile aussi d'utiliser *Terraform* et ansible dans la gestion de l'infrastructure.

## Amélioration du livrable CD
Dans ce fil rouge, le projet déploie un *Helm Chart* et non pas une image Docker.
Cela permettra non seulement de déployer une image mais aussi sa configuration. 
Je trouve les Helms charts plus flexibles à utiliser en les comparant avec Docker Compose.

### Point à remédier - GitOps
Actuellement, le repo *Git* ne décrit pas l'image déployée. Ce point doit changer dans d'autres itération en manipulant 
les "releases" ou les tags de *GitLab* ou permettre à la Chaine d'intégration de déployer sur une branche de production.
Et c'est pour cette raison que je n'ai pas travaillé avec *ArgoCD*, parce que j'ai évité d'utiliser un outil de *GitOps* alors que mon repo n'est pas prêt.

## Infrastructure as Code – *Terraform*
J'ai utilisé *Terraform* pour déployer des ressources sur *OpenStack* ainsi que *AWS*. Et je peux confirmer que chaque plateforme à ces spécificités lors de la déclaration de l'infrastructure. AWS a beaucoup plus de "Ressources" est donc il est possible de déployer la même chose mais avec plus d'étapes.
Ce projet a nécessité de déployer un Internet Gateway et de configurer le tableau de routage du *Public Subnet* d'une manière explicite.
Cette étape était importante pour donner l'accès en entrer au serveur. Sans mes connaissances sur ce fournisseur de cloud, je ne pourrais pas automatiser le déploiement de l'instance.

### Points à corriger
Il faut ajouter des variables dans la déclaration de l'infrastructure pour permettre plus de flexibilité comme la configuration de la clé publique de connexion.
Il faut aussi travailler sur une architecture multi-nœuds, avec un *Application Load Balancer ALB* qui est cloud native pour plus de robustesse.

## Configuration - Ansible
Terraform, à l'aide du **local exec** liste l'IP publique de l'instance déployée. Elle sera introduite par le script d'automatisation dans l'inventaire d'*Ansible*.
Le playbook est un seul fichier ou les instructions sont spécifiées.
J'ai trouvé au début des problèmes avec l'installation du Runner sur l'instance mais c'est résolu.
### Points à corriger
Il faut penser à automatiser la configuration du secret de *GitLab*, qui permet de télécharger les images privées.
Il faut aussi travailler sur la création de rôles pour diviser l'installation du Runner de l'installation de Docker et K8S.

## Suppression du Reverse Proxy
J'ai opté pour l'ingress de K8S et donc je n'ai plus besoin de mon composant reverse proxy.
La configuration de l'ingress est facile et marche très bien avec l'esprit de K8S.
### Points à remédier
Il faut voir comment exposer une application en HTTPS avec l'ingress.

## Conclusion
Ce projet m’a permis de manipuler plus les technologies *Kubernetes*, *Ansible*, *Terraform*. Je trouve que la gestion de l'infrastructure et de la configuration en code est importante dans une approche Devops. Ceci permet la reproductibilité et le sauvegarde des changements.
Je trouve que K8S est une outil puissant extensible avec les projets tel que *Helm* ou *ArgoCD*. J'aime la flexibilité qu'il l'offre. Peut-être qu'il représente un gros morceau 
à apprendre mais avec de la pratique, on arrive à voir les réflexes élémentaires dans une courte période.
