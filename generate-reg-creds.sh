#!/bin/bash

echo  -n "Registry URL: "
read URL
echo  -n "Email: "
read EMAIL
echo  -n "Login: "
read USERNAME
echo  -n "Password: "
read -s PASSWORD

export DOCKER_AUTH_STRING=$(echo -n "$USERNAME:$PASSWORD" | base64 -w 0)
export URL
export USERNAME
export EMAIL
export PASSWORD

DOCKER_CONFIG=$(cat templates/.dockerconfigjson.template | envsubst '${URL}, ${USERNAME}, ${PASSWORD}, ${EMAIL}, ${DOCKER_AUTH_STRING}')
export DOCKER_CONFIG_SECRET=$(echo -n $DOCKER_CONFIG | base64 -w 0)

mkdir tmp
cat templates/registry-credentials.yml.template | envsubst '${DOCKER_CONFIG_SECRET}' > tmp/secret.yml
microk8s kubectl apply -f tmp/secret.yml
rm tmp/secret.yml