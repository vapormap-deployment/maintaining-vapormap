

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.large"

  tags = {
    Name = "Fil-Rouge"
  }
  key_name = aws_key_pair.instance_key.key_name

  network_interface {
    network_interface_id = aws_network_interface.foo.id
    device_index         = 0
  }
  
  depends_on = [aws_internet_gateway.gw]

  credit_specification {
    cpu_credits = "unlimited"
  }


}

