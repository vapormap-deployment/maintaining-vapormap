
resource "aws_vpc" "my_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "tf-example"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "TF test gw"
  }
}



resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = true

}

resource "aws_network_interface" "foo" {
  subnet_id   = aws_subnet.my_subnet.id
  private_ips = ["172.16.10.100"]
  security_groups = [aws_security_group.allow_ssh_and_http.id]
}

# Routing - For public Subnet


resource "aws_default_route_table" "public" {
  default_route_table_id = "${aws_vpc.my_vpc.main_route_table_id}"

  tags = {
    Name = "subnet-rt-public"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_default_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw.id}"

  timeouts {
    create = "5m"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_default_route_table.public.id
}