output "public_ip" {
  description = "The public IP address assigned to the instance."
  value       = try(aws_instance.web.public_ip, "")
  depends_on  = [
      aws_instance.web
  ]
}

resource "null_resource" "configure_instance_2" {
  depends_on = [aws_instance.web]

  provisioner "local-exec" {
    command = "echo ${aws_instance.web.public_ip} >> ../tmp/public_ips.txt"
  }
}